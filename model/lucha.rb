class Lucha
  PUNTAJE = {
    'humano' => 1,
    'vampiro' => 2
  }.freeze

  def pelear(personaje1, personaje2)
    return personaje2 if PUNTAJE[personaje1] < PUNTAJE[personaje2]

    'empate'
  end
end
