require 'spec_helper'
require_relative '../model/lucha'

describe 'Lucha' do
  before :each do
    @lucha = Lucha.new
  end

  context 'humano vs' do
    before :each do
      @personaje1 = 'humano'
    end

    context 'humano' do
      before :each do
        @personaje2 = 'humano'
      end

      it 'es un empate' do
        # cuando se enfretan
        resultado = @lucha.pelear(@personaje1, @personaje2)
        # entonces el resultado es un empate
        expect(resultado).to eq('empate')
      end
    end

    context 'vampiro' do
      before :each do
        @personaje2 = 'vampiro'
      end

      it 'gana el vampiro' do
        # cuando se enfretan
        resultado = @lucha.pelear(@personaje1, @personaje2)
        # entonces el resultado es gana vampiro
        expect(resultado).to eq('vampiro')
      end
    end
  end
end
